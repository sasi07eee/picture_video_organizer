.. PictureVideoFileOrganizer documentation master file, created by
   sphinx-quickstart on Sun Jan 17 23:50:12 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Picture and Video File Organizer
================================

This module allows you to organize your picture and video files into directories
that are organized by date.

Typically, there are multiple devices being used to take pictures and videos these
days:

* iPhone
* iPad
* Digital Camera
* Scanned Pictures
* etc.

This module allows you to process the pictures and videos from these different source
directories and organize them all together into two destination directories that are
organized by date:

*Pictures*

* 2017_01_02
* 2017_01_03
* 2017_01_06
* etc.

*Videos*

* 2017_01_02
* 2017_01_05
* 2017_01_08
* etc.

This module utilizes the metadata associated with your picture and video files to determine
the date that the pictures or videos were taken.  This date is then used to determine which
destination sub-directory the file should be stored in.

The following file formats are currently supported:

* JPEG
* MP4
* MOV

Configuration
-------------

The configuration file needs to be edited prior to using this module in order to define where
to look for the source files and where to store the destination files.  The following parameters
should be updated in the *configuration.py* file:

* SOURCE_DIRECTORIES
* DESTINATION_PICTURES_DIRECTORY
* DESTINATION_VIDEOS_DIRECTORY

This configuration file is automatically read in by the module when it is executed.

Execution
---------
How to run this module:

    >>> python picture_video_organizer.py

This will result in all the directories specified by SOURCE_DIRECTORIES being searched for supported
picture and video files and then these are organized into the DESTINATION_PICTURES_DIRECTORY and
DESTINATION_VIDEOS_DIRECTORY appropriately.

The API Documentation / Guide
-----------------------------

If you are looking for information on a specific function, class, or method,
this part of the documentation is for you.

.. toctree::
   :maxdepth: 2

   api
