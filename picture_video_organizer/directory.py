# -*- coding: utf-8 -*-
"""
Specifies a directory contains pictures and videos to be processed.

.. module:: directory
    :synopsis: module defining a directory containing files to be processed.

.. moduleauthor:: Patrick Kennedy <patkennedy79@gmail.com>
"""
from os import listdir
from os.path import isfile, isdir, join
from .file import File, PictureFile, VideoFile


class Directory(object):
    """Defines a directory containing files to be processed.

    This class defines a directory that contains any number of files (without sub-folders) that
    will be copied from this directory to the destination directories.

    The constructor checks that the path for the source directory and the path for the two
    destination directories (pictures and videos) are valid.  Next, all the files
    based on the source directory, all of the picture and video files are identified
    and sorted.  Finally, the parameters about the copy operation for this class are
    initialized to zeros.

    :param directory_path: path of this directory (without the trailing '/')
    :param picture_destination_directory: directory where the picture files will be attempted to be
                                          copied to (without the trailing '/')
    :param video_destination_directory: directory where the video files will be attempted to be
                                        copied to (without the trailing '/')
    :param files: list of Files that are contained in this directory
    """

    def __init__(self, directory_path, picture_destination_directory, video_destination_directory):
        """Initialize the parameters for the directory."""
        self.directory_path = File.check_directory_name(directory_path)
        self.picture_destination_directory = File.check_directory_name(picture_destination_directory)
        self.video_destination_directory = File.check_directory_name(video_destination_directory)
        self.files = [f for f in listdir(self.directory_path) if isfile(join(self.directory_path, f))]
        self.files.sort()
        self.files_copied = 0
        self.files_not_copied = 0
        self.files_with_date_found = 0
        self.files_without_date_found = 0

    def print_details(self):
        """Print the details about the source directory, including the copy results."""
        print '  Directory details:'
        print '    Directory path: {}'.format(self.directory_path)
        print '    Picture Destination directory: %s' % self.picture_destination_directory
        print '    Movie Destination directory: %s' % self.video_destination_directory
        print '    Number of files in directory: %s' % len(self.files)
        print '      Files copied: {}, Files not copied: {} (sum: {})'.format(self.files_copied,
                                                                              self.files_not_copied,
                                                                              len(self.files))
        print '      Date found: {}, Date not found: {} (sum: {})'.format(self.files_with_date_found,
                                                                          self.files_without_date_found,
                                                                          len(self.files))

    def copy_files_to_destination_directory(self):
        """Copy the files from the source directory to the destination directories (picture and video).

        For each file in the source directory, check the extension of the file to determine
        if it should be handled as a picture or a video file.

        Additionally, data about whether or not the copy was successful and whether or not the
        date of the file was found are stored.

        """
        for file in self.files:
            print file
            if file.lower().endswith('.jpg'):
                current_file = PictureFile(file, self.directory_path, self.picture_destination_directory)
                current_file.copy_to_destination_directory()
                current_file.print_details()
            elif file.lower().endswith('.mov') or file.lower().endswith('.mp4'):
                current_file = VideoFile(file, self.directory_path, self.video_destination_directory)
                current_file.copy_to_destination_directory()
                current_file.print_details()
            else:
                current_file = File(file, self.directory_path, self.video_destination_directory)
                print "file extension not found"

            if current_file.copy_successful:
                self.files_copied += 1
                if current_file.get_date_found():
                    self.files_with_date_found += 1
                else:
                    self.files_without_date_found += 1
            else:
                self.files_not_copied += 1


class DirectoryWithDateSubfolders(object):
    """Defines a directory containing sub-folders with dates.

    This class defines a directory that contains any number of directories that are
    named by date that will be copied from this directory to a destination directory.

    For example, this would be the structure of the directory:

    - digital_camera_files
        - 2015_01_01
        - 2015_01_02
        - 2015_01_03
        - 2015_01_07
        - ...

    The constructor checks that the path for the source directory and the path for the two
    destination directories (pictures and videos) are valid.  Next, all the files
    based on the source directory, all of the picture and video files are identified
    and sorted.  Finally, the parameters about the copy operation for this class are
    initialized to zeros.

    :param directory_path: path of this directory
    :param picture_destination_directory: directory where the picture files will be attempted to be
                                          copied to (without the trailing '/')
    :param video_destination_directory: directory where the video files will be attempted to be
                                        copied to (without the trailing '/')
    :param directories: list of sub-folders that are contained in this directory

    .. note::
        The directories that are passed into the Constructor can either have a
        trailing '/' or not.  Within the Constructor, there is a check to ensure
        that a trailing '/' is included in the directory string.
    """

    def __init__(self, directory_path, picture_destination_directory, video_destination_directory):
        """Initialize the parameters for the directory with data subfolders."""
        self.directory_path = File.check_directory_name(directory_path)
        self.picture_destination_directory = File.check_directory_name(picture_destination_directory)
        self.video_destination_directory = File.check_directory_name(video_destination_directory)
        self.directories = [f for f in listdir(self.directory_path) if isdir(join(self.directory_path, f))]
        self.directories.sort()
        self.file_count = 0
        self.files_copied = 0
        self.files_not_copied = 0
        self.files_with_date_found = 0
        self.files_without_date_found = 0

    def print_details(self):
        """Print the details about the source directory, including the copy results."""
        print "  Directory details:"
        print "    Directory path: %s" % self.directory_path
        print "    Picture Destination directory: %s" % self.picture_destination_directory
        print "    Video Destination directory: %s" % self.video_destination_directory
        print "    Number of directories in directory: %s" % len(self.directories)
        print "    Number of files in directory: %s" % self.file_count
        print '      Files copied: {}, Files not copied: {} (sum: {})'.format(self.files_copied,
                                                                              self.files_not_copied,
                                                                              self.file_count)
        print '      Date found: {}, Date not found: {} (sum: {})'.format(self.files_with_date_found,
                                                                          self.files_without_date_found,
                                                                          self.file_count)

    def copy_files_to_destination_directory(self):
        """Copy the files from the source directory to the destination directories (picture and video).

        For each file in the source directory, check the extension of the file to determine
        if it should be handled as a picture or a video file.

        Additionally, data about whether or not the copy was successful and whether or not the
        date of the file was found are stored.

        """
        for current_folder_original in self.directories:
            if current_folder_original.lower().startswith('20'):
                current_dir = Directory(self.directory_path + current_folder_original,
                                        self.picture_destination_directory,
                                        self.video_destination_directory)
                current_dir.copy_files_to_destination_directory()
                current_dir.print_details()

                self.file_count += len(current_dir.files)
                self.files_copied += current_dir.files_copied
                self.files_not_copied += current_dir.files_not_copied
                self.files_with_date_found += current_dir.files_with_date_found
                self.files_without_date_found += current_dir.files_without_date_found
