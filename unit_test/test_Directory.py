# from picture_video_organizer.directory import Directory
from picture_video_organizer import Directory
from os import listdir


class TestDirectory:
    def test_constructor(self):
        source_dir_test = "./unit_test/source_dir/Camera Roll"
        picture_destination_dir_test = "./unit_test/destination_dir/Pictures"
        movie_destination_dir_test = "./unit_test/destination_dir/Movies/"
        test_dir = Directory(source_dir_test, picture_destination_dir_test, movie_destination_dir_test)

        assert test_dir.directory_path == "./unit_test/source_dir/Camera Roll/"
        assert test_dir.picture_destination_directory == "./unit_test/destination_dir/Pictures/"
        assert test_dir.video_destination_directory == "./unit_test/destination_dir/Movies/"
        assert len(test_dir.files) == 9
        assert test_dir.files_copied == 0
        assert test_dir.files_not_copied == 0
        assert test_dir.files_with_date_found == 0
        assert test_dir.files_without_date_found == 0

    def test_copy(self):
        source_dir_test = "./unit_test/source_dir/Camera Roll"
        picture_destination_dir_test = "./unit_test/destination_dir/Pictures"
        movie_destination_dir_test = "./unit_test/destination_dir/Movies/"
        test_dir = Directory(source_dir_test, picture_destination_dir_test, movie_destination_dir_test)
        test_dir.copy_files_to_destination_directory()

        assert test_dir.directory_path == "./unit_test/source_dir/Camera Roll/"
        assert test_dir.picture_destination_directory == "./unit_test/destination_dir/Pictures/"
        assert test_dir.video_destination_directory == "./unit_test/destination_dir/Movies/"
        assert len(test_dir.files) == 9
        assert test_dir.files_copied == 8
        assert test_dir.files_not_copied == 1
        assert test_dir.files_with_date_found == 4
        assert test_dir.files_without_date_found == 4

        test_files = {
            "IMG_6084.JPG" : "./unit_test/destination_dir/Pictures/no_date_available",
            "IMG_6272.JPG" : "./unit_test/destination_dir/Pictures/no_date_available",
            "IMG_6285.JPG" : "./unit_test/destination_dir/Pictures/2015-07-17",
            "IMG_6296.JPG" : "./unit_test/destination_dir/Pictures/2015-07-17",
            "IMG_6367.JPG" : "./unit_test/destination_dir/Pictures/no_date_available",
            "IMG_6385.JPG" : "./unit_test/destination_dir/Pictures/no_date_available",
            "IMG_6781.JPG" : "./unit_test/destination_dir/Pictures/2015-08-06",
            "IMG_6784.JPG" : "./unit_test/destination_dir/Pictures/2015-08-06"
        }

        for key, value in test_files.items():
            assert (key in listdir(value))

    def test_copy_second_time(self):
        source_dir_test = "./unit_test/source_dir/Camera Roll"
        picture_destination_dir_test = "./unit_test/destination_dir/Pictures"
        movie_destination_dir_test = "./unit_test/destination_dir/Movies/"
        test_dir = Directory(source_dir_test, picture_destination_dir_test, movie_destination_dir_test)
        test_dir.copy_files_to_destination_directory()

        assert test_dir.directory_path == "./unit_test/source_dir/Camera Roll/"
        assert test_dir.picture_destination_directory == "./unit_test/destination_dir/Pictures/"
        assert test_dir.video_destination_directory == "./unit_test/destination_dir/Movies/"
        assert len(test_dir.files) == 9
        assert test_dir.files_copied == 0
        assert test_dir.files_not_copied == 9
        assert test_dir.files_with_date_found == 0
        assert test_dir.files_without_date_found == 0

        test_files = {
            "IMG_6084.JPG" : "./unit_test/destination_dir/Pictures/no_date_available",
            "IMG_6272.JPG" : "./unit_test/destination_dir/Pictures/no_date_available",
            "IMG_6285.JPG" : "./unit_test/destination_dir/Pictures/2015-07-17",
            "IMG_6296.JPG" : "./unit_test/destination_dir/Pictures/2015-07-17",
            "IMG_6367.JPG" : "./unit_test/destination_dir/Pictures/no_date_available",
            "IMG_6385.JPG" : "./unit_test/destination_dir/Pictures/no_date_available",
            "IMG_6781.JPG" : "./unit_test/destination_dir/Pictures/2015-08-06",
            "IMG_6784.JPG" : "./unit_test/destination_dir/Pictures/2015-08-06"
        }

        for key, value in test_files.items():
            assert (key in listdir(value))

    def test_print_details(self):
        source_dir_test = "./unit_test/source_dir/Camera Roll"
        picture_destination_dir_test = "./unit_test/destination_dir/Pictures"
        movie_destination_dir_test = "./unit_test/destination_dir/Movies/"
        test_dir = Directory(source_dir_test, picture_destination_dir_test, movie_destination_dir_test)
        test_dir.print_details()

        assert True
