# -*- coding: utf-8 -*-
from os.path import isdir
from datetime import datetime
from picture_video_organizer import Directory
from picture_video_organizer import DirectoryWithDateSubfolders

"""Set the source directories where the files (pictures and videos) will be
 copied from.
"""
source_directories = {
    "Patrick_iPhone6s": '/Users/patrickkennedy/Pictures/Patrick_iPhone6s/Camera Roll',
    "iPad": '/Users/patrickkennedy/Pictures/iPad2',
}

source_directories_camera = {
    # "Digital_Camera": '/Users/patrickkennedy/Pictures/Digital_Camera'
}

"""
Set the destination directories where the pictures will be stored and where
 the videos will be stored.
"""
pictures_destination_directory = '/Users/patrickkennedy/Pictures/Pictures'
videos_destination_directory = '/Users/patrickkennedy/Movies/Movies'

print("Picture and Video File Organizer Application...")

# Set the start time
start_time = datetime.now()

for key, value in source_directories.items():
    print "Processing files from %s..." % key
    if isdir(value):
        current_directory = Directory(value, pictures_destination_directory, videos_destination_directory)
        current_directory.copy_files_to_destination_directory()
        current_directory.print_details()
    else:
        print "Error! Directory does not exist: %s" % value

for key, value in source_directories_camera.items():
    print "Processing files from %s..." % key
    if isdir(value):
        current_directory = DirectoryWithDateSubfolders(value, pictures_destination_directory, videos_destination_directory)
        current_directory.copy_files_to_destination_directory()
        current_directory.print_details()
    else:
        print "Error! Directory does not exist: %s" % value

# Set the end time
end_time = datetime.now()
elapsed_time = end_time - start_time
print("Elapsed time: %s" % elapsed_time)
